import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, Events } from 'ionic-angular';

import { StatusBar, Splashscreen, NativeStorage } from 'ionic-native';

import { HomePage } from '../pages/home/home';
import { ConversationsPage } from '../pages/conversations/conversations';
import { LoginPage } from '../pages/login/login';
import { SingleConversationPage } from '../pages/single-conversation/single-conversation';
import { CachePlaceMatchesPage } from '../pages/cache-place-matches/cache-place-matches';
import { AngularFire } from 'angularfire2';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;
  //required to get nav controller in MyApp - the template features a nav element so injection is not available
  //must use view child to get the nav element
   @ViewChild('myNav') nav;

  constructor(platform: Platform, public af: AngularFire, public events: Events) {
    const authObserver = af.auth.subscribe( user => {
      if (user) {
        this.rootPage = HomePage;
        console.log(user);
        authObserver.unsubscribe();
      } else {
        this.rootPage = LoginPage;
        authObserver.unsubscribe();
      }
    });
    let env = this;
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      /*let nav = this.nav;
      NativeStorage.getItem('user')
        .then( function (data) {
          env.rootPage = HomePage;
          Splashscreen.hide();
          console.log("got it");
        }, function (error) {
          env.rootPage = LoginPage;
          console.log("NOT got it");
          Splashscreen.hide();
        });*/
      this.events.subscribe('event:logout', ()=>{
        const ANIMATION = { animate: true, direction: 'back' };
        this.nav.setRoot(LoginPage, null, ANIMATION);
      });

      this.events.subscribe('event:viewMatches', (viewMatchEventData)=>{
        const ANIMATION = { animate: true, direction: 'forward' };
        console.log("AT EVENT, CACHE TO VIEW MATCHES: ");
        console.log(viewMatchEventData);
        this.nav.push(CachePlaceMatchesPage, {
          cacheID: viewMatchEventData.cacheID
        });
      });

      this.events.subscribe('event:viewConversation', (viewConversationEventData)=>{
        const ANIMATION = { animate: true, direction: 'forward' };
        console.log("View Conv EVENT: ");
        console.log(viewConversationEventData.conv);
        this.nav.push(SingleConversationPage, { conv: viewConversationEventData.conv });
      });

      this.events.subscribe('conversation:blockedChange', (blockedChangeEventData)=>{

        console.log("conv blocked EVENT: ");
        console.log(blockedChangeEventData);
        this.nav.push(HomePage, {tabTo: blockedChangeEventData.tabTo});
      });



      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }
}
