import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Camera } from '@ionic-native/camera';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { ConversationsPage } from '../pages/conversations/conversations';
import { AddImagePopoverPage } from '../pages/add-image-popover/add-image-popover';
import { FullscreenImgModalPage } from '../pages/fullscreen-img-modal/fullscreen-img-modal';
import { SingleConversationPage } from '../pages/single-conversation/single-conversation';
import { SingleConversationPopoverPage } from '../pages/single-conversation-popover/single-conversation-popover';
import { ProfilePage } from '../pages/profile/profile';
import { CachePlaceMapPage } from '../pages/cache-place-map/cache-place-map';
import { EditProfilePage } from '../pages/edit-profile/edit-profile';
import { CachePlaceMatchesPage } from '../pages/cache-place-matches/cache-place-matches';
import { AuthData } from '../providers/auth-data';
import { Database } from '../providers/database';
import { AutocompletePagePage } from '../pages/autocomplete-page/autocomplete-page';
import { NearbyPlacesModalPage } from '../pages/nearby-places-modal/nearby-places-modal';
import { Maps } from '../providers/maps';
import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2';

export const firebaseConfig = {
  apiKey: "AIzaSyDjpajSbzkRHkkKUW1OhhlvEm3aY0oK6Wo",
  authDomain: "test1-80e8d.firebaseapp.com",
  databaseURL: "https://test1-80e8d.firebaseio.com",
  storageBucket: "test1-80e8d.appspot.com",
  messagingSenderId: "262029500301"
};

export const myFirebaseAuthConfig = {
  provider: AuthProviders.Password,
  method: AuthMethods.Password
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    SignupPage,
    ConversationsPage,
    ProfilePage,
    CachePlaceMapPage,
    CachePlaceMatchesPage,
    AutocompletePagePage,
    NearbyPlacesModalPage,
    SingleConversationPage,
    SingleConversationPopoverPage,
    FullscreenImgModalPage,
    EditProfilePage,
    AddImagePopoverPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig, myFirebaseAuthConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    SignupPage,
    ConversationsPage,
    ProfilePage,
    CachePlaceMapPage,
    CachePlaceMatchesPage,
    AutocompletePagePage,
    NearbyPlacesModalPage,
    SingleConversationPage,
    SingleConversationPopoverPage,
    FullscreenImgModalPage,
    EditProfilePage,
    AddImagePopoverPage
  ],
  providers: [
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Maps,
    AuthData,
    Database,
    Camera
  ]
})
export class AppModule {}
