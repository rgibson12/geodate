import { Injectable, Inject } from '@angular/core';
import { AuthProviders, AngularFire, FirebaseAuthState, AuthMethods, FirebaseApp } from 'angularfire2'; //Add FirebaseApp
import { Platform } from 'ionic-angular';
import { Facebook, NativeStorage } from 'ionic-native';
import { Observable } from "rxjs/Observable";
import { auth, database } from 'firebase';

@Injectable()
export class AuthData {
  fireAuth: any;
  firebase: any;

  constructor(public af: AngularFire, @Inject(FirebaseApp)firebase: any,private platform: Platform) {
    af.auth.subscribe(user => {
      if (user) {
        this.fireAuth = user.auth;
        console.log(user);
      }
    });
    this.firebase = firebase;
  }

  loginUser(newEmail: string, newPassword: string): any {
    return this.af.auth.login({ email: newEmail, password: newPassword });
  }

  logoutUser(): any {
    return this.af.auth.logout();
  }

  signupUser(newEmail: string, newPassword: string): any {
    return this.af.auth.createUser({ email: newEmail, password: newPassword });
  }

  loginUserFb(){
  //return Observable.create(observer => {
    return Facebook.login(['email', 'public_profile']).then(res => {
      let userId = res.authResponse.userID;
      let params = new Array();

      Facebook.api("/me?fields=first_name,gender,age_range", params)
      .then(function(user) {
        user.picture = "https://graph.facebook.com/" + userId + "/picture?type=large";
        var canvas = document.createElement('canvas');
        canvas.width = 200;
        canvas.height = 200;

        var img = new Image();
        img.setAttribute('crossOrigin', 'anonymous'); //for toDataURL
        img.src = user.picture;
        img.height = 200;
        img.width = 200;
        var data64URL = img.src;
        img.onload = function() {
          var ctx = canvas.getContext("2d");
          /*ctx.drawImage(document.getElementById("works"), 0, 0);
          console.log(canvas.toDataURL());*/
          ctx.drawImage(img, 0, 0);
          data64URL = canvas.toDataURL().replace(/^data:image\/(png|jpg);base64,/, '');
          if (data64URL){
            console.log("BASE64 FB IMAGE DATA:");
            console.log(data64URL);
            user.picture = data64URL;
          }
          NativeStorage.setItem('user',
          {
            name: user.first_name,
            age: user.age_range,
            gender: user.gender,
            picture: data64URL,
            fbid: userId
          });
        }
        document.body.appendChild(img);
        console.log("adding user image to storage: " + data64URL);
        //now we have the users info, let's save it in the NativeStorage
        NativeStorage.setItem('user',
        {
          name: user.first_name,
          age: user.age_range,
          gender: user.gender,
          picture: data64URL,
          fbid: userId
        });
      });

      const facebookCredential = auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
      return this.firebase.auth().signInWithCredential(facebookCredential);
    });
}

}
