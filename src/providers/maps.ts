import { Injectable, NgZone } from '@angular/core';
import { Http } from '@angular/http';
import { Geolocation } from 'ionic-native';
import 'rxjs/add/operator/map';
import { AlertController, Alert, Events } from 'ionic-angular';
import { CachePlaceMatchesPage } from '../pages/cache-place-matches/cache-place-matches';
import { Database } from './database'
import { Observable } from 'rxjs/Rx'
import {
 GoogleMap,
 GoogleMapsEvent,
 GoogleMapsLatLng,
 CameraPosition,
 GoogleMapsMarkerOptions,
 GoogleMapsMarker
} from 'ionic-native';
declare var google:any;
/*
  Generated class for the Maps provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Maps {
  map: any;
  nearbyPlaces = [];
  markers = [];
  toDelete: boolean = false;
  infoWindow: any;
  //places service needs a html element to bind to - normally the map itself but seeing as theres no map on this page...
  service = new google.maps.places.PlacesService(document.createElement('div'));

  constructor(public http: Http , public _ngZone: NgZone, public alertCtrl: AlertController, public db: Database, public events: Events/*private alertCtrl: AlertController*/) {
    console.log('Hello Maps Provider');
    window["angularComponentRef"] = { component: this, zone: this._ngZone };
  }

  loadMap(cacheData){
    var location = new GoogleMapsLatLng(54.5137689,-2.2954714);
    this.map = new google.maps.Map(document.getElementById('map'), {
    zoom: 5,
    center: location,
    zoomControl: false,
    streetViewControl: false
  });

    this.centerOnUserLocation();
    this.addCacheMarkers(cacheData);

  }

  removeMarker(cacheEventData){
    console.log("markers before: ");
    console.log(this.markers);
    console.log("received marker to remove:");
    console.log(cacheEventData);
    var index = this.markers.indexOf(cacheEventData.marker);
    this.markers[index].setMap(null);
    this.markers.splice(index,1);
      console.log("markers after: ");
    console.log(this.markers);

  }

  centerOnUserLocation(){
    let mapItem = this.map;
      //location can also be retrieved using maps
      if(navigator.geolocation){
        navigator.geolocation.getCurrentPosition(function(resp) {
          let location = new GoogleMapsLatLng(resp.coords.latitude, resp.coords.longitude);
          let markerOptions: GoogleMapsMarkerOptions = {
            position: location,
            title: 'Me'
          };
          mapItem.panTo(location);
          console.log("at location HTML5");
          }, function() {
            console.log("location service failed");
          });
      } else {
        console.log("html5 location not supported - trying cordova plugin");
        Geolocation.getCurrentPosition().then((resp) => {
          let location = new GoogleMapsLatLng(resp.coords.latitude, resp.coords.longitude);
          let markerOptions: GoogleMapsMarkerOptions = {
            position: location,
            title: 'Me'
          };
          mapItem.panTo(location);
          console.log("at location CORDOVA");
        }).catch((error) => {
          console.log('Error getting location', error);
        });
      }


  }

  deleteOn(){
    this.toDelete = true;
    console.log("ToDelete is: " + this.toDelete);
  }

  addCacheMarkers(cacheData){
    if (cacheData){
      let mapItem = this.map;
      let me = this;
      var index = 0;
      cacheData.forEach(function(element) {
        let cacheLoc = new GoogleMapsLatLng(element.location.lat, element.location.long);
        /*let markerOptions: GoogleMapsMarkerOptions = {
          position: cacheLoc,
          title: element.name,
          snippet: "Added on: " + element.creationDate
        };
        mapItem.addMarker(markerOptions, function(marker){
          marker.showInfoWindow();
        })*/
        me.addMarker(element);
        element.cacheIndex = index;
        index++;
        //me.markers.push(element);
      });
    }
  }

  addMarker(data){
    let cacheLoc = new GoogleMapsLatLng(data.location.lat, data.location.long);
    let me = this;
    let markerOptions = {
      position: cacheLoc,
      map: me.map,
      title: data.name,
      snippet: "Added on: " + data.creationDate,
    };
    var marker = new google.maps.Marker(markerOptions);
    marker.placeID=data.placeID;
    marker.addListener('click', function() {
      var infoWindow = new google.maps.InfoWindow({
        content:''
      });
      if(me.infoWindow){
        me.infoWindow.close();
      }
      window["angularComponentRef"].marker=marker;
      me.infoWindow=infoWindow;
      infoWindow.setContent(
        '<h2>'+data.name+'</h2>'+
        '<p>Added on: '+data.creationDate+'</p>'+
       '<button style="background-color: #387ef5; '+
                      'border: none;'+
                      'color: white;'+
                      'padding: 10px 25px;'+
                      'text-align: center;'+
                      'text-decoration: none;'+
                      'display: inline-block;'+
                      'font-size: 12px;'+
                      'margin: 4px 2px;'+
                      'cursor: pointer;'+
                      '" onclick="window.angularComponentRef.zone.run(() => {window.angularComponentRef.component.viewMatches(window.angularComponentRef.marker);})">View Matches</button>' +
        '<button style="background-color: #f20202; '+
                       'border: none;'+
                       'color: white;'+
                       'padding: 10px 25px;'+
                       'text-align: center;'+
                       'text-decoration: none;'+
                       'display: inline-block;'+
                       'font-size: 12px;'+
                       'margin: 4px 2px;'+
                       'cursor: pointer;'+
                       '" onclick="window.angularComponentRef.zone.run(() => {window.angularComponentRef.component.deleteCacheCheck(window.angularComponentRef.marker);})">Delete</button>'
    );

      infoWindow.open(me.map, marker);
    });
    me.markers.push(marker);
  }

  viewMatches(marker){
    let me = this;
    console.log("viewing matches for cache: " + marker.placeID);

    this.events.publish('event:viewMatches', {
      cacheID: marker.placeID
    });
  }

  deleteCacheCheck(marker){
    let me = this;
    console.log("MEdb: " + me.db);
    console.log("RECEIVED MARKER:");
    console.log(marker);
    let alert = this.alertCtrl.create({
      title: 'Delete cache?',
      message: 'You wont get matched with others here anymore',
      buttons: [{
        text: 'Yes',
        handler: ()=>{
          console.log("PRESSED YES");
          me.db.deleteCache(marker);
        }
      }, {
        text: 'Cancel',
        handler: ()=>{
          console.log("PRESSED CANCEL ");

        }
      }]
    });
    alert.present();
  }

  loadPlaces(loading: any){
    let me=this;
    return new Observable(observer=>{
      console.log("LOADING PLACES");
      var request;
      //let me = this;
      me.nearbyPlaces = [];
      Geolocation.getCurrentPosition().then((resp) => {
        console.log("got location CORDOVA plugin");
        request  = {
          location: new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude),
          radius: '300'
        };
        //console.log("GOT THESE LOCATION COORDS: + " + resp.coords.latitude + ", " + resp.coords.longitude);

        var callback1 = (function(results, status){
          if (status == google.maps.places.PlacesServiceStatus.OK) {
            for (var i = 0; i < results.length; i++) {
              me.nearbyPlaces.push({name: results[i].name,
                                    address: results[i].vicinity,
                                    icon: results[i].icon,
                                    uid: results[i].place_id,
                                    location: {
                                      lat: results[i].geometry.location.lat(),
                                      long: results[i].geometry.location.lng()
                                    }});
              //console.log(results[i]);
            }
          }
          loading.dismiss();
        }).bind(me);
        //jiggery pokery to gain access to 'this' as the component - within the callback 'this' is undefined

        me.service.nearbySearch(request, callback1);
        console.log(me.nearbyPlaces);
        observer.next(me.nearbyPlaces);

      }).catch((error) => {
        console.log('Error getting location with CORDOVA PLUGIN - trying HTML5 service', error);
        if(navigator.geolocation){
          navigator.geolocation.getCurrentPosition(function(resp) {
            console.log("got location HTML5");
            request  = {
              location: new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude),
              radius: '300'
            };
            //console.log("GOT THESE LOCATION COORDS: + " + resp.coords.latitude + ", " + resp.coords.longitude);

            var callback1 = (function(results, status){
              if (status == google.maps.places.PlacesServiceStatus.OK) {
                for (var i = 0; i < results.length; i++) {
                  me.nearbyPlaces.push({name: results[i].name,
                                        address: results[i].vicinity,
                                        icon: results[i].icon,
                                        uid: results[i].place_id,
                                        location: {
                                          lat: results[i].geometry.location.lat(),
                                          long: results[i].geometry.location.lng()
                                        }});
                  //console.log(results[i]);
                }
              }
            }).bind(me);
            //jiggery pokery to gain access to 'this' as the component - within the callback 'this' is undefined

            me.service.nearbySearch(request, callback1);
            console.log(me.nearbyPlaces);
            observer.next(me.nearbyPlaces);
            }, function() {
              console.log("html5 location service failed");
              observer.catch();
            });
        } else {
          console.log("html5 location not supported");

        }
      });








    })

  }


}
