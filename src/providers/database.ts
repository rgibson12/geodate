import { Injectable, Inject } from '@angular/core';
import { Facebook, NativeStorage } from 'ionic-native';
import { Events } from 'ionic-angular'
import { AngularFire, FirebaseApp, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2';
import { database } from 'firebase';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx'
import 'rxjs/add/operator/map';

/*
  Generated class for the Database provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Database {
  db: any;
  events: any;
  authdUser: any;
  constructor(@Inject(FirebaseApp)firebase: any, events: Events, public af: AngularFire) {
    console.log('Hello Database Provider');
    this.db=firebase.database();
    this.events= events;

  }

  deleteImage(imgKey, fbUID){
    console.log("deleting image " + imgKey + " for user " + fbUID);
    var img = this.af.database.object("Users/"+fbUID+"/Images/"+imgKey);
    img.remove();
  }

  getUserProfile(fbUID){
    return this.af.database.object("Users/"+fbUID+"/Profile");
  }

  getUserProfileRAW(fbUID){
    let refPath = "Users/"+fbUID+"/Profile";
    return this.db.ref(refPath).once('value');
  }

  getCaches(fbUID){
    return this.af.database.list("Users/"+fbUID+"/Caches");
  }

  getUserImages(fbUID){
    return this.af.database.list("Users/"+fbUID+"/Images");
  }

  getUserImagesArray(fbUID){
    var imgs = this.af.database.list("Users/"+fbUID+"/Images", { preserveSnapshot: true });
    var imgsArray = [];
    imgs.subscribe(snapshot=>{
      console.log("imags found");
      var counter = 0;
      snapshot.forEach(snap=>{
        var imgObj = snap.val();
        imgObj.counter = counter;
        imgsArray.push(imgObj);
        counter++;
      })
      //imgsArray = this.snapshotToArray(snapshot);

    })
    return new Promise (function(resolve, reject) {
      console.log(imgsArray);
      resolve(imgsArray);
    });
  }



  getMatches(cacheID){
    var matches = this.af.database.list("CachePlaces/"+cacheID, { preserveSnapshot: true });
    var profiles = [];
    let me = this;
    matches.subscribe(snapshots=>{
    //  profiles=[];
      snapshots.forEach(snapshort => {
        console.log("key" + snapshort.key);
        var userKey = snapshort.key;
        var profile = me.af.database.object("Users/"+snapshort.key+"/Profile", { preserveSnapshot: true });
        profile.subscribe(snapshot2=>{
          console.log(snapshot2.key);
          console.log(snapshot2.val());
          var prof = snapshot2.val();
          prof.user = userKey;
          profiles.push(prof);
        })
      })
    })
    return new Promise (function(resolve, reject) {
      resolve(profiles);
    });
  }


  snapshotToArray(snapshot){
    var object = [];
    snapshot.forEach(subSnap => {
      //console.log("Indexing in array:");
      //console.log(subSnap.val());
        object.push(subSnap.val());
    });
    return object;
  }


  getConversations(fbid){
    console.log("getting converstaions, user:" + fbid);
    var converstaions = this.af.database.list("Users/"+fbid+"/Conversations", { preserveSnapshot: true });
    var convProfiles = [];
    console.log("found some convs");
    let me = this;
    var convObject;
    converstaions.subscribe(snapshots=>{
    //  profiles=[];
      snapshots.forEach(snapshort => {
        convObject = {
          conversation: null,
          recipProfile: null,
          lastMessage: null,
          creationDate: null
        };

        console.log("key" + snapshort.key);
        var conversationID = snapshort.key;
        convObject.conversation = conversationID;

        var conv = me.af.database.object("Conversations/"+conversationID, { preserveSnapshot: true });
        conv.subscribe(snapshot2=>{
          console.log(snapshot2.key);
          console.log(snapshot2.val());
          convObject.lastMessage = snapshot2.val().lastMessage;
          convObject.creationDate = snapshot2.val().creationDate;
        });


        var convMem = me.af.database.list("ConversationMembers/"+conversationID, { preserveSnapshot: true });
        convMem.subscribe(snapshot3=>{
        //  console.log(snapshot3.key);
        //  console.log(snapshot3.val());
        //  var participants = snapshot3.val();

          snapshot3.forEach(snapshot4 => {
            var userKey = snapshot4.key;
            console.log("user key: " + userKey);
            console.log("authd user: " + fbid );
            if (userKey != fbid){
              var profile = me.af.database.object("Users/"+userKey+"/Profile", { preserveSnapshot: true });
              profile.subscribe(snapshot5=>{
                console.log(snapshot5.key);
                console.log(snapshot5.val());

                convObject.recipProfile = snapshot5.val();
                console.log("convObject: ");
                console.log(convObject);
                convProfiles.push(convObject);
              })
            }
          });

          //prof.user = userKey;
          //convProfiles.push(prof);
        })
      })
    })
    console.log(convProfiles);
    return new Promise (function(resolve, reject) {
      resolve(convProfiles);
    });
  }

  getMessages(conv){
    return this.af.database.list("Messages/"+conv);
  }

  conversationBetween(users){
    let refPath = "Users/" + users.authdUser + "/ConversationsWith/"+users.receivingUser;
    return this.db.ref(refPath).once('value');
  }

  createConversation(users){
    console.log("called create conversation");
    var conversations = this.af.database.list("Conversations");
    let me = this;

    conversations.push({
      blocked: {
        isBlocked: false,
        blockedBy: null
      },
      creationDate: new Date().toString(),
      lastMessage: ""
    }).then((newConv) => {
      console.log(newConv.key);
      var conversationMemAuthdUser = this.af.database.object("ConversationMembers/"+newConv.key+"/"+users.authdUser);
      var conversationMemRecUser = this.af.database.object("ConversationMembers/"+newConv.key+"/"+users.receivingUser);
      var authdUserConvs = this.af.database.object("Users/"+users.authdUser+"/Conversations/"+newConv.key);
      var receivingUserConvs = this.af.database.object("Users/"+users.receivingUser+"/Conversations/"+newConv.key);
      var authdUserConvsWith = this.af.database.object("Users/"+users.authdUser+"/ConversationsWith/"+users.receivingUser);
      var receivingUserConvsWith = this.af.database.object("Users/"+users.receivingUser+"/ConversationsWith/"+users.authdUser);

      conversationMemAuthdUser.set(true).then(success=>{
        conversationMemRecUser.set(true).then(success=>{
          authdUserConvs.set(true).then(success=>{
            receivingUserConvs.set(true).then(success=>{
              authdUserConvsWith.set(newConv.key).then(success=>{
                receivingUserConvsWith.set(newConv.key).then(success=>{
                  var profile = me.getUserProfileRAW(users.receivingUser);
                  if (profile){
                    profile.then(function(snapshot2){
                      me.events.publish('event:viewConversation', {
                        conv: {
                          conversation: newConv.key,
                          recipProfile: snapshot2.val()
                        }
                      });
                    });
                  }
                });
              });
            });
          });
        });
      });
    });
  }

  getConversations2(fbid){
    let me = this;

    //var convObject;
    return new Observable(observer=> {
      var profiles = [];
      var conversations = this.af.database.list("Users/"+fbid+"/ConversationsWith", {preserveSnapshot: true});
      conversations.subscribe(snapshot=>{
        snapshot.forEach(snap => {
          var conv = snap.val();
          //console.log("Conv: " + conv + ". User: " + snap.key);
          //convObject.conversation = conv;
          var profile = me.af.database.object("Users/"+snap.key+"/Profile", { preserveSnapshot: true });
          profile.subscribe(snap2=> {
            console.log(snap2.val());
            var prof = snap2.val();
            prof.conversation = conv;
            console.log(prof);
            profiles.push(prof);
            //console.log(convObject);
          })
        });
      })
      console.log(profiles);
      observer.next(profiles);
    })


  }

  deleteCache(marker){
    return NativeStorage.getItem("user").then(data=>{
      let userId = data.fbid;
      let cacheID = marker.placeID;
      let refPath = '/CachePlaces/'+cacheID+'/'+userId;
      let env = this;

      return this.db.ref(refPath).once('value').then(function(snapshot) {
        if(snapshot.val()){
          //console.log("Place already cached exists");
          //console.log(snapshot.val());
          return env.db.ref(refPath).remove(()=>{
            refPath = '/Users/'+userId+'/Caches/'+cacheID;
            return env.db.ref(refPath).remove(()=>{
              env.events.publish('cache:deleted', {
                marker: marker
              });
            });
          });
        } else {

        }
      });
    });
  }

  createSkeletonProflile(){
    return NativeStorage.getItem("user").then(data=>{
      let userId = data.fbid;
      let refPath = '/Users/'+userId;
      let env = this;

      return this.db.ref(refPath).once('value').then(function(snapshot) {
        if(snapshot.val()){
          console.log("User exists");
          console.log(snapshot.val());
        } else {
          console.log("User does not exist; adding user");
          env.db.ref(refPath).set({
            fbUID : userId,
            Profile: {
              age: data.age.min,
              gender: data.gender,
              mainImg: data.picture,
              name: data.name,
              message: "Hi there, I'm new to geoM8!"
            },
            Images: {
              1: {
                imgSrc: data.picture
              }
            }
          });
        }
      });
    });
  }

  getAllCaches(){
    let env = this;
    var data2;
    console.log("getting caches");
    return new Promise(function(resolve, reject){
      NativeStorage.getItem("user").then(data=>{
        let userId =data.fbid;//10209859935028542;
        let refPath = '/Users/'+userId+'/Caches';
        //data = null;
        env.db.ref(refPath).once('value').then(function(snapshot) {
          data2 = snapshot.val();
          console.log(data2);
          if(data2){
            console.log("DATA2: ");
            console.log(data2);
              var returnData = []
            snapshot.forEach(function(childSnapshot){
              returnData.push(childSnapshot.val());
            })
            resolve(returnData);
          } else {
            resolve();
          }
        });
      });
    })
  }

  getConversation(convID){
    return this.db.ref("Conversations/"+convID).once('value');


  }

  updateLastMessage(msg, conv){
    console.log("conv: " + conv);
    console.log("msg: " + msg);
    this.af.database.object("Conversations/"+conv+"").update({lastMessage: msg});
  }

  blockConversation(convID, toBlock){
    console.log("blocking conversation: " + convID);
    return this.af.database.object("Conversations/" + convID + "/blocked").set(toBlock)
    .then(_ => {
      console.log("blocked successfully");
      return true})
    .catch(err=>{
      console.log(err);
      return false;
    })
  }

  createCache(place){
    console.log("CALLED CREATE CACHE");

    let placeID = place.uid;
    let location = place.location;
    let name = place.name;
    let address = place.address;
    let env =this;

    return NativeStorage.getItem("user").then(data=>{
      let userId = data.fbid;//112816832575790;

      let refPath = '/CachePlaces/'+placeID+'/'+userId;
      return env.db.ref(refPath).set(true).then(()=>{
        refPath = '/Users/'+userId+'/Caches/'+placeID;
        return env.db.ref(refPath).set({
          creationDate: new Date().toString(),
          placeID: placeID,
          location: {
            lat: location.lat,
            long: location.long
          },
          name: name,
          address: address
        }).then(data=>{
          env.events.publish('cache:created', {
            creationDate: new Date().toString(),
            placeID: placeID,
            location: {
              lat: location.lat,
              long: location.long
            },
            name: name,
            address: address
          });
          return true;
        });

      });
    });
  }
}
