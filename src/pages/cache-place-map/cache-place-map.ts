import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, Platform, ModalController, Events, AlertController } from 'ionic-angular';
import { Maps } from '../../providers/maps';
import { NearbyPlacesModalPage } from '../nearby-places-modal/nearby-places-modal';
import { Database } from '../../providers/database';
import {
 GoogleMap,
 GoogleMapsEvent,
 GoogleMapsLatLng,
 CameraPosition,
 GoogleMapsMarkerOptions,
 GoogleMapsMarker
} from 'ionic-native';


/*
  Generated class for the Profile page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-cache-place-map',
  templateUrl: 'cache-place-map.html'
})
export class CachePlaceMapPage {
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  toDelete: boolean;
  caches: any;
  infowindow: any;
  address;


  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public navParams: NavParams, public platform: Platform, public map2: Maps, private modalCtrl: ModalController, public db: Database, public events: Events) {
    this.address = {
      place: ''
    };
    platform.ready().then(() => {
      let me = this;
      me.caches = [];
      this.db.getAllCaches().then(function(res){
        console.log(res);
        if(res){
          me.caches= res;
        }
        me.map2.loadMap(res);
      });

      this.events.subscribe('cache:created', (cacheEventData) => {
        me.map2.addMarker(cacheEventData);
        me.caches.push(cacheEventData);
        //navCtrl.setRoot(me);
        //this.navCtrl.setRoot(ProfilePage, {cache: cacheEventData});
      });

      this.events.subscribe('cache:deleted', (cacheEventData) => {
        me.map2.removeMarker(cacheEventData);
        //me.caches.push(cacheEventData);
        navCtrl.setRoot(me);
        //this.navCtrl.setRoot(ProfilePage, {cache: cacheEventData});
      });

        });
  }

  ngAfterViewInit() {
   //this.loadMap();
  }

  showNearbyPlaces () {
    //this.navCtrl.push(NearbyPlacesModalPage);
    let modal = this.modalCtrl.create(NearbyPlacesModalPage);
    let me = this;
    modal.onDidDismiss(data => {
      this.address.place = data;
    });
    modal.present();
  }

  doAThing () {
    this.map2.deleteOn();
    console.log("hey");/*
    let alert = this.alertCtrl.create({
      title: 'Delete cache?',
      message: 'You wont get matched with others here anymore',
      buttons: [{
        text: 'Yes',
        handler: ()=>{
          console.log("PRESSED YES");
        }
      }, {
        text: 'Cancel',
        handler: ()=>{
          console.log("PRESSED CANCEL ");

        }
      }]
    });
    alert.present();*/
  }

  deleteCache() {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');


  }


}
