import { Component, NgZone, ChangeDetectorRef  } from '@angular/core';
import { NavController, NavParams, ViewController, Platform, AlertController, LoadingController, ModalController  } from 'ionic-angular';
import { Geolocation } from 'ionic-native';
import { Database } from '../../providers/database';
import { HomePage } from '../home/home';
import { CachePlaceMapPage } from '../cache-place-map/cache-place-map';
import { Maps } from '../../providers/maps';
declare var google:any;
/*
  Generated class for the NearbyPlacesModal page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-nearby-places-modal',
  templateUrl: 'nearby-places-modal.html'
})
export class NearbyPlacesModalPage {
  nearbyPlaces;
  //places service needs a html element to bind to - normally the map itself but seeing as theres no map on this page...
  service = new google.maps.places.PlacesService(document.createElement('div'));

  constructor(ChangeDetectorRef: ChangeDetectorRef, public navCtrl: NavController, public modalCtrl: ModalController, public viewCtrl: ViewController, private zone: NgZone, public maps: Maps, public platform: Platform, public alertCtrl: AlertController, public db: Database,
    private zone1: NgZone, public loadingCtrl: LoadingController ) {

    /*this.nearbyPlaces.then(()=>{
      console.log("data back");
      ChangeDetectorRef.detectChanges()
    });*/
    platform.ready().then(() => {
          //  this.loadMap();
          let me = this;
          var loading = this.loadingCtrl.create({
            dismissOnPageChange: true,
            content: 'Finding nearby places..',
          });

          this.nearbyPlaces = [];
          this.db = db;


          let geoAlert = this.alertCtrl.create({
            title: 'Geolocation and Google Maps',
            message:'</p> We use Google Maps to find places nearby to you, which requires your location. It is never stored, published or shared.</p> <p>By clicking Accept, you are consenting to provide your devices location and accepting Google Maps Terms of Service.</p> '+
            '<p>Read Google Maps terms of service: https://developers.google.com/maps/terms </p>',
            buttons: [{
              text: 'Accept',
              handler: ()=>{
                loading.present();
                console.log("Accepted location/maps");
                maps.loadPlaces(loading).subscribe(places =>{
                  me.nearbyPlaces = places;
                  me.zone1.run(() => {
                    console.log('running');
                  });
                  geoAlert.dismiss();
                });
              }
            }, {
              text: 'Go back',
              handler: ()=>{
                console.log("Denied location permissions");
                geoAlert.dismiss();
                me.navCtrl.pop();

              }
            }]
          });
          geoAlert.present();



        });
  }


  refreshPlaces(){
    this.viewCtrl.dismiss();
    let modal = this.modalCtrl.create(NearbyPlacesModalPage);
    modal.present();
  }

  chooseItem(item: any) {
    let me = this;
    let alert = this.alertCtrl.create({
      title: 'Add Cache',
      message: 'Add cache to: ' + item.name + '?',
      buttons: [{
        text: 'Yes',
        handler: ()=>{
          console.log("PRESSED YES to: " + item.name);
          me.db.createCache(item).then((res)=>{
            console.log("res" + res);
            if(res){
              console.log("NOW CLOSE THIS PAGE");
              me.navCtrl.pop();
            } else {
              let alert2 = this.alertCtrl.create({
                title: 'No-can-do...',
                message: 'You have already cached this place',
                buttons: [{text:'OK'}]
              });
              alert2.present();
            }
          });
        }
      }, {
        text: 'Cancel',
        handler: ()=>{
          console.log("PRESSED CANCEL to: " + item.name);

        }
      }]
    });

    alert.present();
    //this.viewCtrl.dismiss(item);
  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad NearbyPlacesModalPage');
  }


}
