import { Component } from '@angular/core';
import { NavController, NavParams, PopoverController } from 'ionic-angular';
import { Database } from '../../providers/database';
import { NativeStorage } from 'ionic-native';
import { FormBuilder, Validators } from '@angular/forms';
import { AngularFire, FirebaseApp, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2';
import { SingleConversationPopoverPage } from '../single-conversation-popover/single-conversation-popover';
/*
  Generated class for the SingleConversation page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-single-conversation',
  templateUrl: 'single-conversation.html'
})
export class SingleConversationPage {
  convProfObj: any=null;
  conversation: any={blocked:false};
  messages: FirebaseListObservable<any>;
  authdUser: boolean = false;
  message:any;
  msgForm:any;

  constructor(public formBuilder: FormBuilder, public db: Database, public popoverCtrl: PopoverController, public params: NavParams, public navCtrl: NavController, public navParams: NavParams) {
    NativeStorage.getItem("user").then((data)=>{
      this.authdUser = data.fbid;
    })
    this.convProfObj = this.params.get('conv');
    console.log(this.convProfObj);
    this.messages = this.db.getMessages(this.convProfObj.conversation);
    console.log("MessageS: ");
    console.log(this.messages);
    let me = this;
    this.db.getConversation(this.convProfObj.conversation).then(function(snapshot) {
      me.conversation=snapshot.val();
      me.conversation.convID = snapshot.key;
      console.log("got conversation: ");
      console.log(me.conversation);
    });
    this.msgForm = formBuilder.group({
      messageBody: ['', Validators.compose([Validators.required, Validators.minLength(1)])],
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SingleConversationPage');
  }

  showConversationPopover(event){
    console.log("showConversationPopover clicked");
    let popover = this.popoverCtrl.create(SingleConversationPopoverPage, {conversation: this.convProfObj.conversation});
    popover.present({
      ev: event
    });
  }

  showTimestamp(message){
    if(message.showTimestamp){
        message.showTimestamp = false;
    } else {
      message.showTimestamp = true;
    }
  }

  sendMessage(){
    let env=this;
    if (this.msgForm.valid){
      console.log("message: " + this.msgForm.value.messageBody);
      this.messages.push({
        body: ""+this.msgForm.value.messageBody+"",
        sender: this.authdUser,
        timestamp: new Date().toString()
      }).then(()=>{
        env.db.updateLastMessage(env.msgForm.value.messageBody, env.convProfObj.conversation);
      });

    }

  }

}
