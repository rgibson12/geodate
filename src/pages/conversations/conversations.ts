import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { NativeStorage } from 'ionic-native';
import { SingleConversationPage } from '../single-conversation/single-conversation';
import { Database } from '../../providers/database';
import { AuthProviders, AngularFire, FirebaseAuthState, AuthMethods, FirebaseApp, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2';

/*
  Generated class for the Conversations page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-conversations',
  templateUrl: 'conversations.html'
})
export class ConversationsPage {
  showDetail: any;
  conversations: any;
  matches: FirebaseListObservable<any>;

  constructor(public af: AngularFire, public navCtrl: NavController, public navParams: NavParams, public db: Database, public events: Events) {
    NativeStorage.getItem("user").then(data=>{
      this.db.getConversations2(data.fbid).subscribe(convs =>
        this.conversations = convs);

    });


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConversationsPage');
  }

  viewConversation(conv){
    var convObject = {
      conversation: conv.conversation,
      recipProfile: {name: conv.name, age: conv.age, mainImg: conv.mainImg, gender: conv.gender, message: conv.message},
      lastMessage: null,
      creationDate: null
    };
    console.log("sending conv object to singleConverstaion page: ");
    console.log(convObject);
    //this.navCtrl.push(SingleConversationPage, {conv: conv});
    this.events.publish('event:viewConversation', {conv: convObject});
  }

}
