import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { NativeStorage } from 'ionic-native';
import { ProfilePage } from '../profile/profile';
import { Database } from '../../providers/database';
import { AuthProviders, AngularFire, FirebaseAuthState, AuthMethods, FirebaseApp, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2';
/*
  Generated class for the CachePlaceMatches page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-cache-place-matches',
  templateUrl: 'cache-place-matches.html'
})
export class CachePlaceMatchesPage {
  showDetail: any;
  caches: FirebaseListObservable<any>;
  matches: any;

  constructor(public af: AngularFire, public navCtrl: NavController, public navParams: NavParams, public db: Database, public params: NavParams) {
    let cacheID = this.params.get('cacheID');
    this.matches = db.getMatches(cacheID);
    console.log("VIEW MATCHES FOR: " + cacheID);
    console.log(this.matches);
  }


  viewUserProfile(user){
    console.log("look at profile for: " + user);
    this.navCtrl.push(ProfilePage, {user: user});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CachePlaceMatchesPage');
  }

}
