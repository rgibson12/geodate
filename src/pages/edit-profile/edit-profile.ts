import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, PopoverController, AlertController } from 'ionic-angular';
import { FullscreenImgModalPage } from '../fullscreen-img-modal/fullscreen-img-modal';
import { FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2';
import { AddImagePopoverPage } from '../add-image-popover/add-image-popover';
import { NativeStorage } from 'ionic-native';
import { Database } from '../../providers/database';
/*
  Generated class for the EditProfile page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html'
})
export class EditProfilePage {

  fbImages: any;
  profile: FirebaseObjectObservable<any>;
  images: any = [];
  fbID: any;

  constructor(public db: Database, public alertCtrl: AlertController, public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams, public popoverCtrl: PopoverController) {
    NativeStorage.getItem("user").then((data)=>{
      this.fbID = data.fbid;
      this.profile = db.getUserProfile(data.fbid);
      this.fbImages = db.getUserImages(data.fbid);
      this.fbImages.subscribe((data)=>{
        this.images = data;
        /*if(this.images.length< 8){
          this.images.push({imgSrc: "img/addImg.png", addImgPlaceholder: true})
        }*/
        console.log(this.images);
      })

    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditProfilePage');
  }

  deleteImage(img){
    let alert = this.alertCtrl.create({
      title: 'Delete Image',
      message: 'Delete this image?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Delete',
          handler: () => {
            console.log('Delete clicked');
            console.log("deleting image: "+ img.$key);
            this.db.deleteImage(img.$key, this.fbID);
          }
        }
      ]
    });
    alert.present();
  }

  uploadImage(event){
    console.log("upload image clicked");
    let popover = this.popoverCtrl.create(AddImagePopoverPage);
    popover.present({
      ev: event
    });
  }

  showImage(img){
    var url = img.imgSrc;
    if(!img.addImgPlaceholder){
      console.log("image: " + url);
      let modal = this.modalCtrl.create(FullscreenImgModalPage, {photo: url});
      //this.events.publish('event:fullscreenImg');
      modal.present();
    } else {
      console.log("clicked ADD IMAGE img");
    }
  }

}
