import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { NativeStorage } from 'ionic-native';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthData } from '../../providers/auth-data';
import { LoginPage } from '../login/login';
import { ProfilePage } from '../profile/profile';
import { ConversationsPage } from '../conversations/conversations';
import { CachePlaceMapPage } from '../cache-place-map/cache-place-map';
import { CachePlaceMatchesPage } from '../cache-place-matches/cache-place-matches';
import { EmailValidator } from '../../validators/email';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  tab1Root = ProfilePage;
  tab2Root = CachePlaceMapPage;
  tab3Root = ConversationsPage;
  loading: any;
  user: any= this.params.get('user');
  tabTo: any;
  @ViewChild('myTabs') tabRef: any;

  constructor(public nav: NavController, public authData: AuthData,
    public formBuilder: FormBuilder, public alertCtrl: AlertController,
    public loadingCtrl: LoadingController, public params: NavParams) {

      this.user = this.params.get('user');
      this.tabTo = this.params.get('tabTo');
      console.log("tab to: " + this.tabTo);

      console.log("HOMEPAGE USER: " + this.user);

  }

  ngAfterViewInit() {
    this.tabRef.select(this.tabTo);
  }

    logoutUser(){
      this.authData.logoutUser().then( authData => {
          NativeStorage.remove('user');
          this.nav.setRoot(LoginPage);
    }, error => {
      this.loading.dismiss().then( () => {
        let alert = this.alertCtrl.create({
          message: error.message,
          buttons: [
          {
            text: "Ok",
            role: 'cancel'
          }
          ]
    });
      alert.present();
    });
  });

    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
    });
    this.loading.present();

  }

}
