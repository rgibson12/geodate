import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { Facebook, NativeStorage } from 'ionic-native';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthData } from '../../providers/auth-data';
import { Database } from '../../providers/database';
import { HomePage } from '../home/home';
import { ProfilePage } from '../profile/profile';
import { SignupPage } from '../signup/signup';
import { EmailValidator } from '../../validators/email';

/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})

export class LoginPage {
  loginForm: any;
  emailChanged: boolean = false;
  passwordChanged: boolean = false;
  submitAttempt: boolean = false;
  loading: any;

  constructor(public nav: NavController, public authData: AuthData,
    public formBuilder: FormBuilder, public alertCtrl: AlertController,
    public loadingCtrl: LoadingController, public db: Database)  {

    }


  doFbLogin(){
    this.authData.loginUserFb().then((success) => {
      this.db.createSkeletonProflile().then(()=>{
        NativeStorage.getItem("user").then(data=>{
          console.log("SENDING USER TO HOMEPAGE: " + data.fbid);
            this.nav.setRoot(HomePage, {user: data.fbid, tabTo: 1});
        });
      });

    }, error => {
      this.loading.dismiss().then( () => {
        let alert = this.alertCtrl.create({
          message: error.message,
          buttons: [{
            text: "Ok",
            role: 'cancel'
          }]
        });
        alert.present();
      });
    });

    this.loading = this.loadingCtrl.create({
      dismissOnPageChange: true,
      content: 'Logging in...',
    });
    this.loading.present();
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }


}
