import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ViewController, Events } from 'ionic-angular';
import { NativeStorage } from 'ionic-native'
import { Database } from '../../providers/database';
import { ConversationsPage } from '../conversations/conversations';
import { HomePage } from '../home/home';

/*
  Generated class for the SingleConversationPopover page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-single-conversation-popover',
  templateUrl: 'single-conversation-popover.html'
})
export class SingleConversationPopoverPage {

  conversation: any = {
    blocked:false
  };
  viewer: any;

  constructor(public navCtrl: NavController, public events: Events, public navParams: NavParams, public alertCtrl: AlertController, public db: Database, public viewCtrl: ViewController) {
    let me = this;
    this.db.getConversation(this.navParams.get('conversation')).then(function(snapshot) {
      me.conversation=snapshot.val();
      me.conversation.convID = snapshot.key;
      console.log("got conversation to block: ");
      console.log(me.conversation);
    });
    NativeStorage.getItem("user").then((data) =>{
      me.viewer = data.fbid;
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SingleConversationPopoverPage');
  }

  blockConversation(){
    let me = this;
    let alert = this.alertCtrl.create({
      title: 'Block conversation?',
      message: 'You wont send or receive messages to this conversation. This can be undone at a later time.',
      buttons: [{
        text: 'Yes',
        handler: ()=>{
          console.log("PRESSED YES to block conversation");
          me.db.blockConversation(me.conversation.convID, {
            isBlocked: true,
            blockedBy:me.viewer
          }).then((res)=>{
            console.log("res " + res);
            if(res){
              console.log("NOW CLOSE THIS PAGE");
              me.viewCtrl.dismiss();
              me.events.publish('conversation:blockedChange', {tabTo: 4});
            } else {
              let alert2 = this.alertCtrl.create({
                title: 'No-can-do...',
                message: 'This conversation could not be blocked',
                buttons: [{text:'OK'}]
              });
              alert2.present();
            }
          });
        }
      }, {
        text: 'Cancel',
        handler: ()=>{
          console.log("PRESSED CANCEL to block conversation");

        }
      }]
    });

    alert.present();
  }

  unblockConversation(){
    let me = this;
    let alert = this.alertCtrl.create({
      title: 'Unblock conversation?',
      message: 'You will be able to send and receive messages to this conversation again.',
      buttons: [{
        text: 'Yes',
        handler: ()=>{
          console.log("PRESSED YES to unblock conversation");
          me.db.blockConversation(me.conversation.convID, {
            isBlocked: false,
            blockedBy:null
          }).then((res)=>{
            console.log("res " + res);
            if(res){
              console.log("NOW CLOSE THIS PAGE");
              me.viewCtrl.dismiss();
              me.events.publish('conversation:blockedChange', {tabTo: 4});
            } else {
              let alert2 = this.alertCtrl.create({
                title: 'No-can-do...',
                message: 'This conversation could not be unblocked',
                buttons: [{text:'OK'}]
              });
              alert2.present();
            }
          });
        }
      }, {
        text: 'Cancel',
        handler: ()=>{
          console.log("PRESSED CANCEL to unblock conversation");

        }
      }]
    });

    alert.present();
  }

}
