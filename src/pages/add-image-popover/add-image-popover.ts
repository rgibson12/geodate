import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { NativeStorage } from 'ionic-native'
import { Database } from '../../providers/database';

/*
  Generated class for the AddImagePopover page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-add-image-popover',
  templateUrl: 'add-image-popover.html'
})
export class AddImagePopoverPage {
  images: any;
  constructor(private camera: Camera, public db: Database, public navCtrl: NavController, public viewCtrl: ViewController, public navParams: NavParams) {
    NativeStorage.getItem("user").then((data) =>{
      this.images = db.getUserImages(data.fbid);
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddImagePopoverPage');
  }

  uploadImageCamera(){
    console.log("upload image camera");
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.CAMERA,
      allowEdit : true,
      encodingType: this.camera.EncodingType.PNG,
      targetWidth: 500,
      targetHeight: 500,
    }

    this.camera.getPicture(options).then((imageData) => {
      console.log(imageData);
      this.images.push({ imgSrc: imageData });
      this.viewCtrl.dismiss();
    }, (err) => {
      // Handle error
    });
  }

  uploadImageFilesys(){
    console.log("upload image filesys");
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      allowEdit : true,
      encodingType: this.camera.EncodingType.PNG,
      targetWidth: 200,
      targetHeight: 200,
    }

    this.camera.getPicture(options).then((imageData) => {
      console.log(imageData);
      this.images.push({ imgSrc: imageData });
      this.viewCtrl.dismiss();
    }, (err) => {
      // Handle error
    });
  }

}
