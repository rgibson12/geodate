import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/*
  Generated class for the FullscreenImgModal page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-fullscreen-img-modal',
  templateUrl: 'fullscreen-img-modal.html'
})
export class FullscreenImgModalPage {

  imgSrc: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.imgSrc = navParams.get('photo');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FullscreenImgModalPage');
  }

  closeModal(){
    console.log("hiding image");
  }
}
