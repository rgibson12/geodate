import { Component, ViewChild, ElementRef, Injectable, Inject } from '@angular/core';
import { NavController, NavParams, Platform, Events, ModalController } from 'ionic-angular';
import { Facebook, NativeStorage } from 'ionic-native';
import { Maps } from '../../providers/maps';
import { LoginPage } from '../login/login';
import { FullscreenImgModalPage } from '../fullscreen-img-modal/fullscreen-img-modal';
import { EditProfilePage } from '../edit-profile/edit-profile';
import { Database } from '../../providers/database';
import { AuthData } from '../../providers/auth-data';
import { AuthProviders, AngularFire, FirebaseAuthState, AuthMethods, FirebaseApp, FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2';
import { auth, database } from 'firebase';
import {
 GoogleMap,
 GoogleMapsEvent,
 GoogleMapsLatLng,
 CameraPosition,
 GoogleMapsMarkerOptions,
 GoogleMapsMarker
} from 'ionic-native';
/*
  Generated class for the Profile page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {
  //@ViewChild('map') mapElement: ElementRef;
  //map: any;
  //infowindow: any;
  name: any;
  age: any;
  img_url: any;
  gender: any;
  firebase: any;
  profile: FirebaseObjectObservable<any>;
  images: FirebaseListObservable<any>;
  authUserView: boolean = false;
  authdUser: any;
  profileUser: any;

  constructor(public params: NavParams, public af: AngularFire, public navCtrl: NavController, public modalCtrl: ModalController, public navParams: NavParams, @Inject(FirebaseApp)firebase: any, public platform: Platform, public authData: AuthData, public map2: Maps, public db: Database, public events: Events) {
    platform.ready().then(() => {

    });
    let env = this;
    console.log("PROFILE: " + this.params.get('user'));
    //NativeStorage.getItem("user").then(data=>{
    let user = this.params.get('user');
    NativeStorage.getItem("user").then((data)=>{
      if(!user){
        this.profile = db.getUserProfile(data.fbid);
        this.images = db.getUserImages(data.fbid);
        this.authUserView = true;
      } else {
        if (user==data.fbid){
          this.profile = db.getUserProfile(data.fbid);
          this.images = db.getUserImages(data.fbid);
          this.authUserView = true
        } else {
          this.profile = db.getUserProfile(user);
          this.images = db.getUserImages(user);
          this.profileUser = user;
          this.authUserView = false;
        }
      }
      this.authdUser = data.fbid;
    });


    //});
  }

  ngAfterViewInit() {
   //this.loadMap();
  }

  goToConversation(){
    let me = this;
    if (this.authdUser){
      var conversationExists = this.db.conversationBetween({authdUser: this.authdUser, receivingUser: this.profileUser});
      console.log("conversationExists: ");
      console.log(conversationExists)
      if(conversationExists){
        conversationExists.then(function(snapshot) {
          if(snapshot.val()){
            console.log("found a conversation between: " + me.authdUser + " and: " + me.profileUser);
            console.log(snapshot.val());
            var profile = me.db.getUserProfileRAW(me.profileUser);
            if (profile){
              profile.then(function(snapshot2){
                me.events.publish('event:viewConversation', {
                  conv: {
                    conversation: snapshot.val(),
                    recipProfile: snapshot2.val()
                  }
                });
              });
            }
          } else {
            console.log("the users dont have a conversation yet");
            me.db.createConversation({authdUser: me.authdUser, receivingUser: me.profileUser});
          }
        });
      } else {
        //cant get to firebase ref
      }
    }

  }

  editProfile(){
    this.navCtrl.push(EditProfilePage);
  }

  showImage(url){
    console.log("image: " + url);
    let modal = this.modalCtrl.create(FullscreenImgModalPage, {photo: url});
    //this.events.publish('event:fullscreenImg');
    modal.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');

  }

  logout(){
    var nav = this.navCtrl;
    var me = this;
      this.authData.logoutUser().then(() => {
        console.log("logout");
        Facebook.logout()
        .then(function(response) {
          //user logged out so we will remove him from the NativeStorage
          NativeStorage.remove('user');
          me.events.publish('event:logout');
          //nav.setRoot(LoginPage);
        }, function(error){
          console.log(error);
        });
      //  NativeStorage.remove('user');
        //nav.setRoot(LoginPage);
      });
  }

}
